package com.yunhe.rabbitmq.call;

import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.ReturnedMessage;
import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * 自定义回调的类
 * 实现的接口是内部的接口 所以要进行注入
 */
@Component
@Slf4j
public class MyCallBack implements RabbitTemplate.ConfirmCallback,RabbitTemplate.ReturnsCallback {
    //注入
    @Autowired
    private RabbitTemplate rabbitTemplate;

    //这个注解，实在其他bean注入完成之后，才进行注入的
    @PostConstruct
    public void init(){
        rabbitTemplate.setConfirmCallback(this);
        rabbitTemplate.setReturnsCallback(this);
    }
    /**
     *     交换机确认回调方法
     * 1.发送发消息 交换机收到了 回调
         * 1.1 correlationData 保存回调消息的ID相关信息
         * 1.2 交换机收到消息 true
         * 1.2 cause null
        2.发消息 交换机接受失败了 回调
            2.1 correlationData 保存回调消息的ID相关信息
            2.2 交换机收到消息 ack - false
            2.3 cause 失败的原因
     */
    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
       String id= correlationData != null ? correlationData.getId() : "";
        if (ack){
            log.info("交换机已经收到的Id:{}的消息",id);
        }else {
            log.info("交换机还没有收到id为:{}的消息，由于原因:{}",id,cause);
        }
    }

    /**
     *
     * @param message
     * @param replyCode
     * @param replyText
     * @param exchange
     * @param routingKey
     */

    /**
     * 交换机回退方法
     * 可以在消息在传递过程中达不到目的地是可以进行回退方发
     */

    @Override
    public void returnedMessage(ReturnedMessage returned) {
        log.error("消息{},被交换机{}退回,退回原因:{},路由key:{}",new String(
                returned.getMessage().getBody()),returned.getReplyCode(),returned.getReplyText(),returned.getRoutingKey()
        );
    }


}
