package com.yunhe.rabbitmq.consumer;

import com.yunhe.rabbitmq.config.ConfirmConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 报警交换机
 */
@Component
@Slf4j
public class WarningCosumer {
    //接受报警消息

    @RabbitListener(queues = ConfirmConfig.WARNING_QUEUE_NAME)
    public void  receiveWaringMsg(Message message){
        String s = new String(message.getBody());
        log.error("报警发现不可路由消息:{}",s);
    }
}
