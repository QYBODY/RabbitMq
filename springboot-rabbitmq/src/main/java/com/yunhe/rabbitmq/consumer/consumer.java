package com.yunhe.rabbitmq.consumer;

import com.yunhe.rabbitmq.config.ConfirmConfig;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 接受消息
 */
@Component
@Slf4j
public class consumer {

    @RabbitListener(queues= ConfirmConfig.CONFIRM_QUEUE_NAME)
    public void receiveConfigMessage(Message message){

        log.info("接受到的队列confirm.queue消息:{}",message.getBody());
    }
}
