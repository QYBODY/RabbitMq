package com.yunhe.rabbitmq.demo01;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import java.util.HashMap;
import java.util.Map;

public class Product {
    //队列名称
    private final static String QUEUE_NAME = "hello";
    public static void main(String[] args) {
    //创建一个连接工厂
        ConnectionFactory factory=new ConnectionFactory();
        factory.setHost("39.105.52.92");
        factory.setUsername("rabbitmq");
        factory.setPassword("rabbitmq");

        //channel实现自动的close的借口 自动关闭 补需要进行显示关闭

        try(//通过连接工厂获取一个连接
                Connection connection=factory.newConnection();
                //通过连接建立一个信道
                Channel channel = connection.createChannel();
                ) {
            /**
             * 生成一个队列
             * 1.队列名称
             * 2.队列里面的消息是否持久化 默认消息存储在内存中
             * 3.该队列是否只供一个消费者进行消费 是否进行共享 true 可以多个消费者消费
             * 4.是否自动删除 最后一个消费者端开连接以后 该队列是否自动删除 true 自动删除
             * 5.其他参数
             */
            Map<String,Object> argumcnts =new HashMap<>();
//            官方允许是在0-255 此处设置10 不要设置过大 浪费cpu 优先级范围不能超过10
            //设置优先级
            argumcnts.put("x-max-priority",10);
            channel.queueDeclare(QUEUE_NAME,false,false,false,argumcnts);
            String message="Hello world";
            for (int i = 0; i < 10; i++) {
                if (i==5){
                    /**
                     * 发送一个消息
                     * 1.发送到那个交换机
                     * 2.路由的 key 是哪个
                     * 3.其他的参数信息 设置优先级
                     * 4.发送消息的消息体
                     */
                    AMQP.BasicProperties properties=new AMQP.BasicProperties().builder().priority(5).build();

                    channel.basicPublish("",QUEUE_NAME,properties,message.getBytes("UTF-8"));
                }
                channel.basicPublish("",QUEUE_NAME,null,message.getBytes("UTF-8"));
            }


            System.out.println("发送消息完毕");


        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
