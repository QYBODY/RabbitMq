package com.yunhe.rabbitmq.demo01;

import com.rabbitmq.client.*;

public class Comsumer {
    private final static String QUEUE_NAME = "hello";
    public static void main(String[] args) throws Exception{

        ConnectionFactory factory=new ConnectionFactory();
        factory.setHost("39.105.52.92");
        factory.setUsername("rabbitmq");
        factory.setPassword("rabbitmq");

        Connection connection = factory.newConnection();

        Channel channel = connection.createChannel();

        System.out.println("等待接受数据。。。");

        //推送的消息如何进行消费的接口进行回调


        DeliverCallback deliverCallback=(consumerTag,delivery)->{
            String message= new String(delivery.getBody());
            System.out.println(message);
        };

        //取消消费的一个回调接口 如在消费的时候队列被删除掉了
        CancelCallback cancelCallback=(consumerTag)->{
            System.out.println("消息消费被中断");
        };

        /**
         * 消费者消费消息
         * 1.消费哪个队列
         * 2.消费成功之后是否要自动应答 true 代表自动应答 false 手动应答
         * 3.消费者未成功消费的回调
         */
        channel.basicConsume(QUEUE_NAME,true,deliverCallback,cancelCallback);

    }
}
