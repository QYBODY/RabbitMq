package com.yunhe.rabbitmq.demo09;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.yunhe.rabbitmq.untils.RabbitMqUtils;

//死信队列实战
public class Consumer02 {
    //死信交换机名称
    private static final String DEAD_EXCHANGE = "dead_exchange";

    public static void main(String[] argv) throws Exception {

        Channel channel = RabbitMqUtils.getChannel();

        System.out.println("等待接收消息.....");

        //声明死信队列
        String deadQueue = "dead-queue";

        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("Consumer02 接收到消息"+message);
        };
        channel.basicConsume(deadQueue, true, deliverCallback, consumerTag -> {
        });
    }
}
