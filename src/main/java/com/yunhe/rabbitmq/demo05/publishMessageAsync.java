package com.yunhe.rabbitmq.demo05;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConfirmCallback;
import com.yunhe.rabbitmq.untils.RabbitMqUtils;

import java.util.UUID;
import java.util.concurrent.ConcurrentNavigableMap;
import java.util.concurrent.ConcurrentSkipListMap;

public class publishMessageAsync {

    public static void main(String[] args) throws Exception {

        publishMessageAsync.publishMessageAsync1();
    }

    public static void publishMessageAsync1() throws Exception {
        try (Channel channel = RabbitMqUtils.getChannel()) {

            String queueName = UUID.randomUUID().toString();

            channel.queueDeclare(queueName, false, false, false, null);
            //开启发布确认
            channel.confirmSelect();

            /**
             * 线程安全有序的一个哈希表，适用于高并发的情况
             * 1.轻松的将序号与消息进行关联
             * 2.轻松批量删除条目 只要给到序列号
             * 3.支持并发访问
             */
            ConcurrentSkipListMap<Long, String> outstandingConfirms = new ConcurrentSkipListMap<>();
            /**
             * 确认收到消息的一个回调
             * 1.消息序列号
             * 2.true 可以确认小于等于当前序列号的消息
             * false 确认当前序列号消息
             * （消息的批量确认）
             */
            ConfirmCallback ackCallback = (sequenceNumber, multiple) -> {
                if (multiple) {
                    //返回的是小于等于当前序列号的未确认消息 是一个 map
                    ConcurrentNavigableMap<Long, String> confirmed = outstandingConfirms.headMap(sequenceNumber, true);
                    System.out.println("消息确认成功"+sequenceNumber);
                    //清除该部分未确认消息
                    confirmed.clear();
                }else{
                    System.out.println("消息确认成功"+sequenceNumber);
                    //只清除当前序列号的消息
                    outstandingConfirms.remove(sequenceNumber);
                }
            };
            /**
             * 监听失败的
             */

            ConfirmCallback nackCallback = (sequenceNumber, multiple) -> {
                String message = outstandingConfirms.get(sequenceNumber);
                System.out.println("发布的消息"+message+"未被确认，序列号"+sequenceNumber);
            };
            /**
             * 添加一个异步确认的监听器
             * 1.确认收到消息的回调 监听成功
             * 2.未收到消息的回调 监听失败
             */
            channel.addConfirmListener(ackCallback, nackCallback);



            long begin = System.currentTimeMillis();

            for (int i = 0; i < 100; i++) {
                String message = "消息" + i;

                /**
                 * channel.getNextPublishSeqNo()获取下一个消息的序列号
                 * 通过序列号与消息体进行一个关联
                 * 全部都是未确认的消息体
                 */
                outstandingConfirms.put(channel.getNextPublishSeqNo(), message);

                channel.basicPublish("", queueName, null, message.getBytes());
                System.out.println("发送消息---"+message);
            }
            long end = System.currentTimeMillis();
            System.out.println("发布" + 100 + "个异步确认消息,耗时" + (end - begin) + "ms");
        }


    }
}
