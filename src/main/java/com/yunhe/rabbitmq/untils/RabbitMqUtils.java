package com.yunhe.rabbitmq.untils;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class RabbitMqUtils {
    //得到一个连接的 channel
    public static Channel getChannel() throws Exception{
        //创建一个连接工厂
        ConnectionFactory factory=new ConnectionFactory();
        factory.setHost("39.105.52.92");
        factory.setUsername("rabbitmq");
        factory.setPassword("rabbitmq");

        Connection connection = factory.newConnection();
        //建立一个信道
        Channel channel = connection.createChannel();
        return channel;
    }
}
