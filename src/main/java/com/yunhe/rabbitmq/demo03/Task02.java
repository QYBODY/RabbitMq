package com.yunhe.rabbitmq.demo03;

import com.rabbitmq.client.Channel;
import com.yunhe.rabbitmq.untils.RabbitMqUtils;

import java.util.Scanner;

public class Task02{
    private static final String TASK_QUEUE_NAME = "ack_queue";
    public static void main(String[] args) throws Exception {

        try (Channel channel=RabbitMqUtils.getChannel()){
            /**
             * 第二个参数，建立队列的时候，是否持久化
             */
            //建立一个队列
            channel.queueDeclare(TASK_QUEUE_NAME,false,false,false,null);
            //从控制台当中接受信息
            Scanner scanner = new Scanner(System.in);
            while (scanner.hasNext()){
                String message = scanner.next();
                /**
                 * 发送一个消息
                 * 1.发送到那个交换机
                 * 2.路由的 key 是哪个
                 * 3.其他的参数信息
                 * 4.发送消息的消息体
                 */
                channel.basicPublish("",TASK_QUEUE_NAME,null,message.getBytes());
                System.out.println("发送消息完成:"+message);
            }
        }
    }
}
