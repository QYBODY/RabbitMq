package com.yunhe.rabbitmq.demo08;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.yunhe.rabbitmq.untils.RabbitMqUtils;

public class ReceivelLogsTopic01 {
    //交换机的名称
    public  static final String EXCHANGE_NAME="topic_logs";
    //接受消息
    public static void main(String[] args) throws Exception {
        //获取一个信道
        Channel channel= RabbitMqUtils.getChannel();
        //声明交换机
        channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.TOPIC);
        //声明一个队列
        String queueName="Q2";
        channel.queueDeclare(queueName,false,false,false,null);
        channel.queueBind(queueName,EXCHANGE_NAME,"*.*.rabbit");
        channel.queueBind(queueName,EXCHANGE_NAME,"lazy.#");
        System.out.println("c2  等待接受消息......");
        DeliverCallback deliverCallback=(var1, var2)->{
            System.out.println(new String(var2.getBody(),"UTF-8"));
            System.out.println("接受队列:"+queueName+",绑定键:"+var2.getEnvelope().getRoutingKey());
        };
        //接受消息
        channel.basicConsume(queueName,true,deliverCallback,consumerTag->{});
    }
}
