package com.yunhe.rabbitmq.demo07;

import com.rabbitmq.client.Channel;
import com.yunhe.rabbitmq.untils.RabbitMqUtils;

import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class ProductDirect {

    private static final String EXCHANGE_NAME = "direct_logs";
    public static void main(String[] argv) throws Exception {

       Channel channel = RabbitMqUtils.getChannel();
      //  channel.queueDeclare("disk",false,false,false,null);


        Scanner scanner=new Scanner(System.in);
        while (scanner.hasNext()){
            String message=scanner.next();
            channel.basicPublish(EXCHANGE_NAME,"error",null,message.getBytes(StandardCharsets.UTF_8));

        }


    }
}
