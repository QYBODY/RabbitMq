package com.yunhe.rabbitmq.demo07;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.DeliverCallback;
import com.yunhe.rabbitmq.untils.RabbitMqUtils;

public class DirectComsumer {
    public static final String EXCHANGE_NAME="direct_logs";
    public static void main(String[] args)throws Exception {
        //通过工具类，获取一个信道
        Channel channel = RabbitMqUtils.getChannel();

        /**
         * 生成一个队列
         * 1.队列名称
         * 2.队列里面的消息是否持久化 默认消息存储在内存中
         * 3.该队列是否只供一个消费者进行消费 是否进行共享 true 可以多个消费者消费
         * 4.是否自动删除 最后一个消费者端开连接以后 该队列是否自动删除 true 自动删除
         * 5.其他参数
         */
       // channel.queueDeclare("console",false,false,false,null);
        String queueName = "console";
        //将交换机和队列进行绑定
        channel.queueBind("console",EXCHANGE_NAME,"info");
        channel.queueBind("console",EXCHANGE_NAME,"warning");

        System.out.println("两个rountKey等待接收消息,把接收到的消息打印在屏幕.....");
        DeliverCallback deliverCallback = (consumerTag, delivery) -> {
            String message = new String(delivery.getBody(), "UTF-8");
            System.out.println("控制台打印接收到的消息"+message);
        };
        channel.basicConsume("console", true, deliverCallback, consumerTag -> { });


    }
}
